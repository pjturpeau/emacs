(pj/dot-emacs-msg "start-of-WEBDEV-SETUP")

;; might be interesting to look at http://www.dzr-web.com/people/darren/projects/emacs-webdev/

;; css-mode
(autoload 'css-mode "css-mode" "Mode for editing CSS files" t)
(setq auto-mode-alist (cons '("\\.css\\'" . css-mode) auto-mode-alist))

;; Javascript
(autoload 'javascript-mode "javascript" "Mode for editing Jascript files" t)
(add-to-list 'auto-mode-alist '("\\.js\\'" . javascript-mode))
                                        ;(require 'javascript-mode)

;; sgml-mode
(require 'sgml-mode)
(add-hook 'sgml-mode-hook
          '(lambda ()
             (setq sgml-xml-mode t)
             (setq sgml-basic-offset 4)
             (setq indent-tabs-mode t)
             (setq fill-column 80)
             (turn-off-auto-fill)))

;; php-mode
(require 'php-mode)
(add-hook 'php-mode-user-hook 'turn-on-font-lock)
(setq php-mode-force-pear t)
(add-hook 'php-mode-user-hook
          '(lambda ()
             (setq tab-width 4)
             (setq c-basic-offset 4)
             (setq indent-tabs-mode nil)))

;; Toggle between PHP & HTML Helper mode.  Useful when working on
;; php files, that can been intertwined with HTML code
(defun pj/toggle-php-html-mode ()
  (interactive)
  "Toggle mode between PHP & HTML Helper modes"
  (cond ((string= mode-name "HTML")
         (php-mode))
        ((string= mode-name "XHTML")
         (php-mode))
        ((string= mode-name "PHP")
         (sgml-mode))
        ((string= mode-name "PHP/la")
         (sgml-mode))))

;; switch between php and html mode
(global-set-key [f5] 'pj/toggle-php-html-mode)


;; html mode
;; (autoload 'html-helper-mode "html-helper-mode" "Yay HTML" t)
;; (setq auto-mode-alist (cons '("\\.html$" . html-helper-mode) auto-mode-alist))
;; (add-hook 'html-helper-mode-hook
;;           (function (lambda ()
;;              (setq html-helper-basic-offset 4))))

;; (setq html-helper-do-write-file-hooks t)
;; (setq html-helper-timestamp-hook
;;       '(lambda ()
;;          (insert "Last Modified: "
;;                  (format-time-string "%Y-%m-%d %H:%M:%S\n"))))


(add-to-list 'load-path "~/Archives/Config/elisp/mmm-mode-0.4.8")
(require 'mmm-mode)
(setq mmm-global-mode 'maybe)

(mmm-add-group
 'fancy-html
 '(
   (html-php-output
    :submode php-mode
    :face mmm-output-submode-face
    :front "<\\?php *echo "
    :back "\\?>"
    :include-front t
    :front-offset 5
    )
   (html-php-tagged
    :submode php-mode
    :face mmm-code-submode-face
    :front "<\\?php"
    :back "\\?>")
   (html-css-attribute
    :submode css-mode
    :face mmm-declaration-submode-face
    :front "styleREMOVEME=\""
    :back "\"")))

;; What files to invoke the new html-mode for?
(add-to-list 'auto-mode-alist '("\\.inc\\'" . sgml-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . sgml-mode))
(add-to-list 'auto-mode-alist '("\\.php[34]?\\'" . sgml-mode))
(add-to-list 'auto-mode-alist '("\\.[sj]?html?\\'" . sgml-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . sgml-mode))


;; What features should be turned on in this html-mode?
(add-to-list 'mmm-mode-ext-classes-alist '(sgml-mode nil html-js))
(add-to-list 'mmm-mode-ext-classes-alist '(sgml-mode nil embedded-css))
(add-to-list 'mmm-mode-ext-classes-alist '(sgml-mode nil fancy-html))

(defun save-mmm-c-locals ()
  (with-temp-buffer
    (php-mode)
    (dolist (v (buffer-local-variables))
      (when (string-match "\\`c-" (symbol-name (car v)))
        (add-to-list 'mmm-save-local-variables `(,(car v) nil ,mmm-c-derived-modes))
        )
      )
    )
  )

(save-mmm-c-locals)

(pj/dot-emacs-msg "end-of-WEBDEV-SETUP")