(pj/dot-emacs-msg "start-of-THEME-SETUP")

;; --------------------------------
;; color theme
;; --------------------------------

(defvar *pj/selected-theme* 'zenburn) ; zenburn, colorful-obsolescence, snowish

(cond
 ;;
 ;; Zenburn theme case
 ;;
 ((equal *pj/selected-theme* 'zenburn)
  ;; load zenburn theme
  (require 'zenburn)
  (color-theme-zenburn)

  ;; colors for zenburn
  (custom-set-faces
   '(linum
     ((t (:inherit shadow :background "#f5f0f0" :foreground "maroon" :height 0.9))))
   `(highlight ((t (:background ,zenburn-bg+1))))
   '(show-paren-match-face ((t (:weight bold :background "#4f4f6f"))))
   '(modeline-buffer-id ((t (:foreground "white"))) t))

  )

 ;;
 ;; Snowish theme case
 ;;
 ((equal *pj/selected-theme* 'snowish)
  (color-theme-snowish)

  (custom-set-faces
   '(tool-bar ((t (:foreground "black" :background "#ece9d8"))))
   '(linum
     ((t (:inherit shadow :background "#f5f0f0" :foreground "maroon" :height 0.9))))

   '(bold ((t (:weight bold))))
   '(mode-line ((t (:foreground "grey" :family "DejaVu Sans Mono"
                                :background "#000080"
                                :box ( :line-width 1 :style released-button)
                                ))))
   '(mode-line-inactive ((t (:inherit mode-line :foreground "grey10" :background "grey70"))))
   '(modeline-buffer-id ((t (:foreground "white"))) t))

  (if (stringp (intern-soft "tabbar-default"))
      (progn
        (set-face-attribute 'tabbar-default nil
                            :family "arial"
                            :background "grey90"
                            :height 0.9)

        (set-face-attribute 'tabbar-unselected nil
                            :family "arial"
                            :foreground "black"
                            :background "grey90"
                            :height 0.9
                            :box '(:line-width 1 :color "grey90" :style nil))

        (set-face-attribute 'tabbar-selected nil
                            :family "arial"
                            :foreground "white"
                            :background "grey45"
                            :height 0.9
                            :box '(:line-width 1 :color "grey45" :style nil))

        (set-face-attribute 'tabbar-button nil
                            :family "arial"
                            :foreground "black"
                            :background "grey90"
                            :height 0.9
                            :box '( :line-width 1 :color "grey90" :style nil))))

  (add-hook 'erc-mode-hook
            (lambda ()
              (set-face-attribute 'erc-timestamp-face nil :foreground "grey55"))))

 ;;
 ;; Colorful obsolescence theme case
;;;
 ((equal *pj/selected-theme* 'colorful-obsolescence)
  (load-library "color-theme-colorful-obsolescence")
  (color-theme-colorful-obsolescence)

  ;; tune the theme with my own colors
  (custom-set-faces
   '(default ((t (:foreground "#E0E0E0" :background "black"))))
   '(trailing-white-space
     ((((class color) (background light)) (:background "red"))
      (((class color) (background dark)) (:background "red"))))
   '(trailing-whitespace ((t (:background "red"))))
   '(highlight
     ((((class color) (background light)) (:background "grey90"))
      (((class color) (background dark)) (:background "grey10"))))
   '(font-lock-builtin-face
     ((((class color) (background dark)) (:foreground "LightSteelBlue"))
      (((class color) (background light)) (:foreground "LightSteelBlue"))))
   '(font-lock-preprocessor-face ((t (:inherit font-lock-builtin-face))))


   '(linum
     ((t (:inherit shadow :background "grey15" :foreground "darkgoldenrod" :height 0.9))))

   '(lazy-highlight
     ((((class color) (background dark)) (:background "darkolivegreen1" :foreground "black"))
      (((class color) (background light)) (:background "darkolivegreen1" :foreground "black"))))
   '(bold ((t (:weight bold))))
   '(mode-line ((t (:foreground "grey" :family "DejaVu Sans Mono" :background "#000050"
                                :box ( :line-width 1 :style released-button)
                                ))))
   '(mode-line-inactive ((t (:inherit mode-line :foreground "grey40" :background "#252525"))))
   '(cursor
     ((((class color) (background dark)) (:foreground "white" :background "white"))))
   '(modeline-buffer-id ((t (:foreground "white"))) t))


  (custom-set-faces
   '(show-paren-match
     ((((class color) (background dark)) (:background "dark green" :foreground "green"))
      (((class color) (background light)) (:background "green" :foreground "darkgreen"))))
   '(show-paren-mismatch
     ((((class color) (background dark)) (:background "dark red" :foreground "pink"))
      (((class color) (background light)) (:background "pink" :foreground "dark red")))))

  ;; customize colors for region
  (custom-set-faces
   '(region
     ((((class color) (background dark)) (:background "midnight blue"))
      (((class color) (background light)) (:background "grey70")))))))

;;   (if (stringp (intern-soft "tabbar-default"))
;;       (progn
;;         (set-face-attribute 'tabbar-default nil
;;                             :family "arial"
;;                             :background "grey15"
;;                             :height 0.8)

;;         (set-face-attribute 'tabbar-unselected nil
;;                             :family "arial"
;;                             :background "grey25"
;;                             :foreground "grey"
;;                             :height 0.8
;;                             :box '(:line-width 1
;;                                                :color "grey25"
;;                                                :style nil))

;;         (set-face-attribute 'tabbar-selected nil
;;                             :family "arial"
;;                             :background "grey45"
;;                             :foreground "white"
;;                             :height 0.8
;;                             :box '(:line-width 1
;;                                                :color "grey45"
;;                                                :style nil))

;;         (set-face-attribute 'tabbar-button nil
;;                             :family "arial"
;;                             :foreground "grey"
;;                             :background "black"
;;                             :height 0.8
;;                             :box '(:line-width 1 :color "black" :style nil))))

;;   (add-hook 'erc-mode-hook
;;             (lambda ()
;;               (set-face-attribute 'erc-prompt-face nil
;;                                   :foreground "yellow" :background "#000050")))

;;   (add-hook 'org-mode-hook
;;             (lambda ()
;;               (set-face-attribute 'org-todo nil
;;                                   :foreground "red2"
;;                                   :bold t
;;                                   :family "DejaVu Sans Mono")
;;               (set-face-attribute 'org-done nil
;;                                   :foreground "#806060"
;;                                   :bold nil
;;                                   :family "DejaVu Sans Mono")))))


;; (if (stringp (intern-soft "tabbar-default"))
;;     (progn
;;       (set-face-attribute 'tabbar-default nil
;;                           :family "arial"
;;                           :background "grey90"
;;                           :height 0.9)

;;       (set-face-attribute 'tabbar-unselected nil
;;                           :family "arial"
;;                           :foreground "black"
;;                           :background "grey90"
;;                           :height 0.9
;;                           :box '(:line-width 1 :color "grey90" :style nil))

;;       (set-face-attribute 'tabbar-selected nil
;;                           :family "arial"
;;                           :foreground "white"
;;                           :background "grey45"
;;                           :height 0.9
;;                           :box '(:line-width 1 :color "grey45" :style nil))

;;       (set-face-attribute 'tabbar-button nil
;;                           :family "arial"
;;                           :foreground "black"
;;                           :background "grey90"
;;                           :height 0.9
;;                           :box '( :line-width 1 :color "grey90" :style nil))))


;; (add-hook 'erc-mode-hook
;;           (lambda ()
;;             (set-face-attribute 'erc-prompt-face nil
;;                                 :foreground "yellow" :background "#000050")))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             (set-face-attribute 'org-todo nil
;;                                  :foreground "red2"
;;                                 :bold t
;;                                 :family "DejaVu Sans Mono")
;;             (set-face-attribute 'org-done nil
;;                                  :foreground "#806060"
;;                                 :bold nil
;;                                 :family "DejaVu Sans Mono")))


(pj/dot-emacs-msg "end-of-THEME-SETUP")
