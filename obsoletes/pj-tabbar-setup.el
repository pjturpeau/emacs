(pj/dot-emacs-msg "start-of-TABBAR-SETUP")

(require 'tabbar)
(tabbar-mode)

;; show all buffers in the tabbar mode
(setq tabbar-buffer-groups-function
      (lambda ()
        (list "All buffers")))

;; remove some specific buffers
(setq tabbar-buffer-list-function
      (lambda ()
        (cl-remove-if
         (lambda (buffer)
           ;; Only show a couple buffers with * in their names
           (or (and (cl-find (aref (buffer-name buffer) 0) " *")
                    (not (equal (buffer-name buffer) "*eshell*"))
                    (not (equal (buffer-name buffer) "*mpg123*")))
               (equal (buffer-name buffer) "calendar.diary")
               (equal (buffer-name buffer) "diary")))
         (buffer-list))))

(setq tabbar-background-color "#959A79");; the color of the tabbar background

(setq tabbar-cycle-scope (quote tabs))
(setq tabbar-use-images nil)

(custom-set-faces
 '(tabbar-default ((t (:inherit variable-pitch :background "#959A79" :foreground "black" :weight bold))))
 '(tabbar-button ((t (:inherit tabbar-default :foreground "dark red"))))
 '(tabbar-button-highlight ((t (:inherit tabbar-default))))
 '(tabbar-highlight ((t (:underline t))))
 '(tabbar-selected ((t (:inherit tabbar-default :background "#95CA59"))))
 '(tabbar-separator ((t (:inherit tabbar-default :background "#95CA59"))))
 '(tabbar-unselected ((t (:inherit tabbar-default)))))

(global-set-key [(meta P)] 'tabbar-backward)
(global-set-key [(meta N)] 'tabbar-forward)

;;;; GROUPING BY MAJOR MODES (to be tested...)

;; (global-set-key (kbd "C-S-p") 'tabbar-backward-group)
;; (global-set-key (kbd "C-S-n") 'tabbar-forward-group)
;; (global-set-key (kbd "C-<") 'tabbar-backward)
;; (global-set-key (kbd "C->") 'tabbar-forward) ;; tabbar.el, put all the buffers on the tabs.

;; (defun tabbar-buffer-groups-by-dir ()
;;   "Put all files in the same directory into the same tab bar"
;;   (with-current-buffer (current-buffer)
;;     (let ((dir (expand-file-name default-directory)))
;;       (cond ;; assign group name until one clause succeeds, so the order is important
;;        ((eq major-mode 'dired-mode)
;;         (list "Dired"))
;;        ((memq major-mode
;;               '(help-mode apropos-mode Info-mode Man-mode))
;;         (list "Help"))
;;        ((string-match-p "\*.*\*" (buffer-name))
;;         (list "Misc"))
;;        (t (list dir))))))

;; (defun tabbar-switch-grouping-method (&optional arg)
;;   "Changes grouping method of tabbar to grouping by dir.
;; With a prefix arg, changes to grouping by major mode."
;;   (interactive "P")
;;   (ignore-errors
;;     (if arg
;;         (setq tabbar-buffer-groups-function 'tabbar-buffer-groups) ;; the default setting
;;       (setq tabbar-buffer-groups-function 'tabbar-buffer-groups-by-dir))))

(pj/dot-emacs-msg "end-of-TABBAR-SETUP")
