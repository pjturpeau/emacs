(pj/dot-emacs-msg "start-of-LOOK-SETUP")

;;; to add in .emacs
      ;;
      ;; look customization
      ;;
      ;;(load-library "pj-look-setup")
      ;;(load-library "pj-theme-setup")
	  
	  
;; --------------------------------
;; color theme
;; --------------------------------

;; load emacs color theme framework
(setq load-path (cons "~/Scripts/elisp/color-theme-6.6.0" load-path))
(require 'color-theme)
(color-theme-initialize)

;; (color-theme-snowish)

;; highlight the cursor line for all buffers
(global-hl-line-mode)

;; Show parenthesis matching
(require 'paren)
(show-paren-mode t)

;; highlight the entire expression
(setq show-paren-style 'expression)

;; highlight marked region
(setq transient-mark-mode t)

;; highligh found items during interactive search
(setq research-highlight t)

;; Turn on font-lock mode
(require 'font-lock)
(setq font-lock-maximum-decoration t)
(global-font-lock-mode t)

;; Use lazy lock for syntax coloring, otherwise large files will not
;; be colored. Also allows large files to load faster. Thanks Andrew
;; Innes
(setq font-lock-support-mode 'lazy-lock-mode)
(setq lazy-lock-defer-on-scrolling nil)
(setq lazy-lock-defer-time 1)
(setq lazy-lock-stealth-time 20)
(setq lazy-lock-stealth-lines 25)
(setq lazy-lock-stealth-verbose nil)
(require 'font-lock)
(require 'lazy-lock)

;; scroll bar mode must be set explicitly here (at least on windows)
(scroll-bar-mode -1)

(pj/dot-emacs-msg "end-of-LOOK-SETUP")
