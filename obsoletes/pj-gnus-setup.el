;;; -*- Mode: Emacs-Lisp -*-
;;;
;;; ~/.gnus
;;;
;;; Configuration file of Pierre-Jean Turpeau

;;;
;;; HOW TO USE GNUS WITH THIS KIND OF CONFIGURATION:
;;;
;;; To retrieve mail from an IMAP server, you have to adjust this
;;; configuration file (i.e. modify mail-sources and even
;;; gnus-secondary-select-methods if needed).
;;;
;;; Then you have to browse the server as a foreign server (B key in
;;; Group Topic buffer) using nnimap method.
;;;

;; pour poster proprement en 8bits (avec les accents et tout)
(setq gnus-default-posting-charset (quote iso-8859-1)
      mm-body-charset-encoding-alist '((iso-8859-1 . 8bit)))

;; displayed message if no news
(setq gnus-no-groups-message "No news is horrible news")

;; set directories
(setq gnus-directory "~/Archives/Config/news")
(setq message-directory "~/Archives/Config/mail")
(setq gnus-article-save-directory "~/Archives/Config/mail")
(setq gnus-directory "~/Archives/Config/news")
(setq gnus-cache-directory "~/Archives/Config/news/cache/")
(setq gnus-cache-active-file "~/Archives/Config/news/cache/active")

;; do not use the auto-save capability
(setq gnus-use-dribble-file nil)

;; set messages and mails signature
(setq message-signature-file "~/Archives/Config/signature.txt")
(setq mail-signature-file "~/Archives/Config/signature.txt")

(if *pj/work-computer-p*
    (progn
      (setq url-using-proxy t)
      (setq url-proxy-services
            '(("http"     . "proxy:8080")
              ("no_proxy" . "^.*\\(corp\\|network\\)\.thales")))

      ;;
      ;; Defines pj/alist-of-proxy-logins which is an associated list of login
      ;; and passwords.
      ;;
      ;;       (setq pj/alist-of-proxy-logins
      ;;             '(("a-login" . "its-password")
      ;;               ("another-login" . "another-password")))
      ;;
      (setq pj/alist-of-proxy-logins
            '(("my-pj-login" . "mypassword")))

      (defvar pj/proxy-login nil)
      (defvar pj/last-proxy-login nil)
      (defvar pj/max-proxy-login-delay 60)

      (defadvice read-string (after pj/read-string-after)
        (setq pj/proxy-login ad-return-value))

      (ad-activate 'read-string)
      (defadvice read-passwd (around pj/read-password-around)
        (let ((found-password (cdr (assoc pj/proxy-login pj/alist-of-proxy-logins))))
          (if (stringp found-password)
              (progn
                (setq ad-return-value found-password)
                (setq pj/proxy-login nil))
            ad-do-it)))

      (ad-activate 'read-passwd)))

;; (defadvice read-string (after rs-after)
;;  (message "is proxy %s" (compare-strings (ad-get-arg 0) 14 26 "Proxy-Access" 0 nil))
;;  (message "is me ? %s" (compare-strings (ad-get-arg 1) 0 nil "ab74465" 0 nil))
;;  (message "read-string: %s %s %s" (ad-get-arg 0) (ad-get-arg 1) ad-return-value))

;; (ad-activate 'read-string)



;; ignore vcard signatures
;; (setq gnus-ignored-mime-types
;;       '("text/x-vcard"))

;; set computer dependent parameters
(if pj/home-computer-p
    (progn
	   ;; set default news server
	   (setq gnus-select-method '(nntp "news.club-internet.fr"))

	   ;; it is now set according to sender and posting style
;;	   (setq smtpmail-smtp-server "smtp.turpeau.net")

	   (setq gnus-local-organization "None")
	   (setq gnus-local-domain "club-internet.fr")
	   (setq user-mail-address "pierrejean@turpeau.net")
	   (setq message-user-organization "None")
	   (setq reply-to "pierrejean@turpeau.net")
	   )
  (if pj/work-computer-p
      (progn

	   ;; set default news server
	   (setq gnus-select-method '(nntp "news.corp.thales"))

	   ;; it is now set according to sender and posting style
;; 	   (setq smtpmail-smtp-server "sxapscan")

	   (setq gnus-local-organization "Thales Aerospace")
	   (setq gnus-local-domain "fr.thalesgroup.com")
	   (setq user-mail-address "pierre-jean.turpeau@fr.thalesgroup.com")
	   (setq message-user-organization "Thales Aerospace")
	   (setq reply-to "pierre-jean.turpeau@fr.thalesgroup.com")
	   )))

;; user full name
(setq user-full-name "Pierre-Jean Turpeau")

;; do not send a copy in case of followup to a newsgroup article
(setq message-default-news-headers "Mail-Copies-To: never\n")

;; Enable topics to display groups
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; do not confirm exit request
(setq gnus-interactive-exit nil)
(setq gnus-prompt-before-saving nil)
(setq gnus-save-killed-list nil)
(setq gnus-save-newsrc-file nil)
(setq message-generate-headers-first t)

;; messages could have unlimited size
(setq message-send-mail-partially-limit nil)

;; configure article display look
(setq gnus-article-display-hook
      '(gnus-article-highlight
		;;gnus-article-hide-pgp
		gnus-article-hide-headers-if-wanted
		gnus-article-hide-boring-headers
		;;gnus-article-de-quoted-unreadable
		gnus-article-strip-leading-blank-lines
		gnus-article-remove-trailing-blank-lines
		gnus-article-strip-multiple-blank-lines
		gnus-article-emphasize))

;; automatic carriage return at end of line
(setq message-mode-hook '(turn-on-auto-fill))

;; set carriage return limit
(setq fill-column 79)

;; we don't want to see html and rich text in message
;; WHAT ABOUT RSS DATA ????!!!
(setq mm-discouraged-alternatives '("text/html" "text/richtext"))

;; display smileys
(setq gnus-treat-display-smileys t)
(add-hook 'gnus-article-display-hook 'gnus-smiley-display t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SENT MAIL LOCATION
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Archive sent mails in etc/mail/archive in a folder (not one file
;;; per mail like nnmail).
(setq gnus-message-archive-method
      '(nnfolder "archive"
				 (nnfolder-inhibit-expiry t)
				 (nnfolder-get-new-mail nil)
				 (nnfolder-active-file "~/Archives/Config/mail/archive/active")
				 (nnfolder-directory "~/Archives/Config/mail/archive/")))

;;(setq nnml-directory "~/Archives/Config/mail/archive/")

;;; archive all messages in etc/mail/archive/sent/
(setq gnus-message-archive-group
      '(("mail" "sent")
		(".*"   "sent")))

;;; Used in conjunction with imap make a double storage of fetched
;;; mails, both localy in private folders and remotely on the imap
;;; server. Must be used with a well configured nnmail-split-method or
;;; with a proper imap fetchflag set on \Deleted (\\Deleted) if you
;;; don't want to keep you mails on the imap server.
(setq gnus-secondary-select-methods
		'((nnfolder "private")
;;		  (nnimap "mes1.pes.detexis.thomson-csf.com")
 		))


(setq mail-sources
	  '((pop :server "pop3.turpeau.net"
			 :port 110
			 :user "xxx@turpeau.net"
			 :password "JahCfQnj")
		))

;; (setq mail-sources
;;        '(
;; 		 (file "/var/mail/pturpeau")

;; 		 (imap
;; 		  :server "myoldserver.com"
;; 		  :user   "trytofind"
;; 		  :password "the-password-needed-by-the-imap-server"
;; 		  :fetchflag "\\Seen")
;; 		 ;; search password in .authinfo file ??
;; 		 ;; .authinfo file example :
;; 		 ;; machine machinename login loginname password thepassword

;; 		 (pop
;; 		  :server "mail"
;; 		  :user "turpeau"
;; 		  :password "see@dot_fetchrc"
;; 		  :program "/usr/freeware/bin/fetchmail")
;; 		 ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; POSTING STYLES
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;; met Gnus change the "From:" line by looking at current group we are in.
(setq gnus-posting-styles
	  '(
		(".*" (address "yo@foo.com"))
		("mail.*" (address "pierrejean@turpeau.net"))
		("ibs" (address "info@ibusinessshop.com"))
		("thales" (address "pierre-jean.turpeau@fr.thalesgroup.com"))))
;; signature-file, organization and name might also be configured

;; Available SMTP accounts.
(defvar smtp-accounts
  '((plain "pierrejean@turpeau.net" "smtp.turpeau.net" 25 "pierrejean@turpeau.net" "")
	(plain "info@ibusinessshop.com" "smtp.ibusinessshop.com" 25  "ibusinesibs3" "")
;; 	(ssl   "me@bar.com" "smtp.bar.com" 587 "user-bar" "pass-bar" "key" "cert")
;; 	(ssl   "me@baz.com" "smtp.baz.com" 587 "user-baz" "pass-baz" "key" "cert")
	))

;; send mail to smtp host through smtpmail temp buffer
(require 'smtpmail)
(setq send-mail-function 'smtpmail-send-it)
(setq message-send-mail-function 'smtpmail-send-it)

;; strictly use email address in the 'From' field so that posting
;; style works...
(setq mail-from-style nil)

(setq smtpmail-debug-info t
      smtpmail-debug-verb t)

(defun set-smtp-plain (server port user password)
  "Set related SMTP variables for supplied parameters."
  (setq smtpmail-smtp-server server
		smtpmail-smtp-service port
		smtpmail-auth-credentials (list (list server port user password))
		smtpmail-starttls-credentials nil)
  (message "Setting SMTP server to `%s:%s' for user `%s'."
		   server port user))

(defun set-smtp-ssl (server port user password key cert)
  "Set related SMTP and SSL variables for supplied parameters."
  (setq starttls-use-gnutls t
		starttls-gnutls-program "gnutls-cli"
		starttls-extra-arguments nil
		smtpmail-smtp-server server
		smtpmail-smtp-service port
		smtpmail-auth-credentials (list (list server port user password))
		smtpmail-starttls-credentials (list (list server port key cert)))
  (message
   "Setting SMTP server to `%s:%s' for user `%s'. (SSL enabled.)"
   server port user))

(defun change-smtp ()
  "Change the SMTP server according to the current from line."
  (save-excursion
    (let ((from
           (save-restriction
             (message-narrow-to-headers)
             (message-fetch-field "from"))))
      (cond
       ((string-match "pierrejean@turpeau.net" from)
        (set-smtp-plain "smtp.foo.com" 25 "me" nil))
       ((string-match "m...@bar.com" from)
        (set-smtp-ssl
         "smtp.bar.com" 587 "m...@bar.com" nil nil nil))
       (t (error "Cannot interfere SMTP information."))))))

(add-hook 'message-send-hook 'change-smtp-NOT-WORKING)



;; (defvar mm-url-predefined-programs
;;   '((lynx "lynx" "-source -pauth=ab74465:Maroon9*")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; IMAP CONFIGURATION
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; do crossposting if several split methods match the mail
(setq nnimap-split-crosspost nil)

;; list of IMAP mailboxes to split
(setq nnimap-split-inbox '("INBOX"))
(setq nnimap-split-predicate "UNDELETED")

;; IMAP split rules could also depends on server and inbox. Refer to
;; documentation for more informations (6.5.1 Splitting in IMAP).
(setq nnimap-split-rule
	  '(("INBOX.Expresso" "^From: Majordomo@ri.silicomp.fr" )
        ("INBOX.Expresso" "^To: expresso-tech@ri.silicomp.fr" )
		("INBOX.Private"  "")
		))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; NNMAIL CONFIGURATION
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; allow the use of long file names
(setq nnmail-use-long-file-names t)

;; remove duplicate message
(setq nnmail-treat-duplicates 'delete)

;; do crossposting if several split methods match the mail
(setq nnmail-crosspost nil)

(setq nnmail-split-methods
      '(("Inbox" "")))

;; (setq nnmail-split-methods
;;       '(("mail.enseirb.i3" "^To:.*i3@enseirb.fr")
;; 		("mail.enseirb.i3" "^Cc:.*i3@enseirb.fr")
;; 		("mail.enseirb.i2" "^To:.*i2@enseirb.fr")
;; 		("mail.enseirb.i2" "^Cc:.*i2@enseirb.fr")
;; 		("mail.enseirb.eleves" "^To:.*eleves@enseirb.fr")
;; 		("mail.enseirb.eleves" "^Cc:.*eleves@enseirb.fr")
;; 		("mail.enseirb.all" "^To:.*all@enseirb.fr")
;; 		("mail.enseirb.all" "^Cc:.*all@enseirb.fr")
;; 		("mail.enseirb.stages" "^Subject:.*[sS][tT][aA][gG][eE].*")
;; 		("mail.enseirb.rsr" "^To:.*rsr.*")
;; 		("mail.enseirb.rsr" "^To:.*[rR][sS][rR].*")
;; 		("mail.enseirb.rsr" "^To:.*[rR][sS][rR]@[eE][nN][sS][eE][iI][rR][bB].*")
;; 		("mail.enseirb.rsr-projet" "^From:.*oudot\.enseirb@wanadoo.fr")
;; 		("mail.enseirb.rsr-projet" "^From:.*oudot\.laurent@wanadoo.fr")
;; 		("mail.enseirb.rsr-projet" "^From:.*yoann@mandrakesoft.com")
;; 		("mail.enseirb.rsr-projet" "^From:.*michael@miknet.net")
;; 		("mail.enseirb.admin" "^From: .*[fF]ran[c�]oise.*[vV]erdier.*")
;; 		("mail.enseirb.admin" "^From: .*[gG]ervais.*")
;; 		("mail.enseirb.admin" "^From: .*[pP]iraube.*")
;; 		("mail.enseirb.admin" "^From: .*[mM]archegay.*")
;; 		("mail.mecacyl" "^From:.*mecacyl.*")
;; 		("mail.jobboom" "^From: .*jobboom.*")
;; 		("mail.yahoogroups.sfbay" "^To:.*[sS][fF][bB][aA][yY].*@[yY][aA][hH][oO][oO][gG][rR][oO][uU][pP][sS].[cC][oO][mM]")
;; 		("mail.yahoogroups" "^From:.*@[yY][aA][hH][oO][oO][gG][rR][oO][uU][pP][sS].[cC][oO][mM]")
;; 		("mail.lesmortels" "^To:.*lesmortels.*")
;; 		("mail.kasenna.hq" "^To:.*hq@kasenna.*")
;; 		("mail.kasenna.engr" "^To:.*engr@kasenna.*")
;; 		("mail.kasenna.everyone" "^To:.*everyone@kasenna.*")
;; 		("mail.kasenna.mb_irix_5.1" "^To:.*mb_irix_5.1@kasenna.com.*")
;; 		("mail.kasenna.a4team" "^To:.*a4_team@kasenna.com.*")
;; 		("mail.kasenna.moi" "^From:.*kasenna.*")
;; 		("mail.technart.net" "^From: *technart\.net.*")
;; 		("mail.stephane.richard" "^From:.*stephane_richard@yahoo.com.*")
;; 		("mail.aymeric.vincent" "^From:.*Aymeric.Vincent@labri.u-bordeaux.fr.*")
;; 		("mail.moi" "^To:.*turpeau.*")
;; 		("mail.enseirb.misc" "")	))

;; set the auto expirable groups
(setq gnus-auto-expirable-newsgroups "Inbox.*")

;; set signature separator
(add-hook 'gnus-mode-hook
		  (lambda ()
			(setq gnus-signature-separator
				  '("^-- $"
                    "^-- *$"
                    "^---*$"))))

;; colorier le buffer de r�ponse utilis� le hook
;; gnus-message-setup-hook n'est document� que dans gnus-msg.el comme
;; quoi, il faut lire les sources parfois
(add-hook 'gnus-message-setup-hook 'font-lock-fontify-buffer)

;; remove signature on replied message
(setq message-cite-function 'message-cite-original-without-signature)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SORTING CONFIGURATION
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; sort the summary buffer by reverse date, then by subject, and
;; finally by author.
(setq gnus-thread-sort-functions
	  '(gnus-thread-sort-by-author
		gnus-thread-sort-by-subject
		(not gnus-thread-sort-by-date)))

;; If we use (for any strange reason) an unthreaded display, we specify
;; the same kind of rules to sort articles.
(setq gnus-article-sort-functions
	  '(gnus-article-sort-by-author
		gnus-article-sort-by-subject
		(not gnus-article-sort-by-date)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; DISPLAY THREAD AS TREE
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(setq gnus-use-trees t
;;      gnus-generate-tree-function 'gnus-generate-horizontal-tree
;;      gnus-tree-minimize-window nil)
;;
;;(gnus-add-configuration
;; '(article
;;   (vertical 1.0
;;	     (horizontal 0.4 (summary 0.6 point) (tree 1.0))
;;	     (article 1.0))))

;(mailcap-add "image/jpeg" "display %s")
;(mailcap-add "image/gif" "display %s")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SET COLORS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (custom-set-faces
;;  '(gnus-header-name-face
;;    ((((class color) (background dark)) (:foreground "PaleGreen"))))
;;  '(message-header-name-face
;;    ((((class color) (background dark)) (:foreground "PaleGreen"))))

;;  '(gnus-header-subject-face
;;    ((((class color) (background dark)) (:foreground "SkyBlue"))))
;;  '(message-header-subject-face
;;    ((((class color) (background dark)) (:foreground "SkyBlue"))))

;;  '(gnus-header-newsgroups-face
;;    ((((class color) (background dark)) (:foreground "Gold"))))
;;  '(message-header-newsgroups-face
;;    ((((class color) (background dark)) (:foreground "Gold"))))

;;  '(gnus-header-from-face
;;    ((((class color) (background dark)) (:foreground "MediumSeaGreen"))))
;;  '(message-header-cc-face
;;    ((((class color) (background dark)) (:foreground "MediumSeaGreen"))))

;;  '(gnus-header-content-face
;;    ((((class color) (background dark)) (:foreground "Grey"))))
;;  '(message-cited-text-face
;;    ((((class color) (background dark)) (:foreground "Grey"))))

;;  '(gnus-signature-face
;;    ((((class color) (background dark)) (:foreground "MediumAquamarine"))))
;;  '(message-header-to-face
;;    ((((class color) (background dark)) (:foreground "MediumAquamarine"))))

;;  '(message-header-other-face
;;    ((((class color) (background dark)) (:foreground "Pink"))))

;;  '(message-header-xheader-face
;;    ((((class color) (background dark)) (:foreground "CornflowerBlue"))))
;;  '(message-mml-face
;;    ((((class color) (background dark)) (:foreground "Khaki"))))
;;  '(message-separator-face
;;    ((((class color) (background dark)) (:foreground "White"))))
;;  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; MISC.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; send message with a delay
(defun wait-seconds (seconds)
  (while (> seconds 0)
    (message " time remaining : %d second(s)" seconds)
    (sleep-for 1)
    (setq seconds (- seconds 1)))
  t)

(defun get-seconds-from-minibuffer ()
  (let ((svalue (read-from-minibuffer "seconds ? ")))
    (let ((nvalue (read-from-string svalue)))
      (car nvalue))))

(defun message-send-later ()
  (interactive)
  (let ((value (get-seconds-from-minibuffer)))
    (wait-seconds value)
    (message-send-and-exit 1)))


;; Hack for posting on foreign servers  (old emacs)

;;(add-hook 'message-setup-hook
;;          (lambda ()
;;            (local-set-key "\C-c\C-c" 'message-send-and-exit-with-prefix)))
;;
;;(defun message-send-and-exit-with-prefix ()
;;  "Call the message-send-and-exit function with a positive number argument
;;to make it post the message on the foreign NNTP server of a group, instead
;;of the default NNTP server"
;;  (interactive)
;;  (message-send-and-exit 1))