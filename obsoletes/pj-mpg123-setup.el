;;; TO ADD in .emacs

;;;
;;; mpeg123 replay capability
;;;
;(if *pj/home-computer-p*
;    (progn
;      (defvar mpg123-default-dir "~/Musique/mp3")
;      (autoload 'pj/music "pj-mpg123-setup" "Launch my music room" t)))


(pj/dot-emacs-msg "start-of-MPG123-SETUP")

(eval-when-compile (require 'mpg123))

(defun pj/music ()
  (interactive)
  (require 'mpg123)

  ;; deactivate play/pause through mouse button 1
  (add-hook 'mpg123-hook (lambda () (local-unset-key (kbd "<down-mouse-1>"))))

  (setq mpg123-mpg123-command "~/Archives/Config/binaries/mpg123-static.exe")
  (setq mpg123-show-help nil)
  (setq mpg123-lazy-slider t)
  (setq mpg123-display-slider nil)

  ;;(setq mpg123-format-name-function 'pj/mpg123-format-name-function)
  (defun pj/mpg123-format-name-function (artist album title tracknum filename)
    (concat (if (or mpg123-omit-id3-artist
                    (string= artist ""))
                ""
              (concat artist " - "))
            (if (string< "" title) title
              filename)))

  (mpg123 nil))

(pj/dot-emacs-msg "end-of-MPG123-SETUP")