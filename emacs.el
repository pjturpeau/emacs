(message " ")
(message "===========[ start-of-EMACS ]===========")
(message " ")


;;
;; loading time
;;
(defvar *pj/emacs-load-start* (current-time))
(defun pj/time-to-ms (time)
	(+ (* (+ (* (car time) (expt 2 16)) (car (cdr time))) 1000000) (car (cdr (cdr time)))))
(defun *pj/display-dot-emacs-timing* ()
	(message ".emacs loaded in %fms" (/ (- (pj/time-to-ms (current-time)) (pj/time-to-ms *pj/emacs-load-start*)) 1000000.0)))
(add-hook 'after-init-hook '*pj/display-dot-emacs-timing* t)

;;
;; standard function to display a message
;;
(defun pj/dot-emacs-msg (msg)
  (message " ")
  (message "---------------[ %s ]------" msg)
  (message " "))

;;
;; prepare parametrization
;;
(defvar *pj/computer-name* (getenv "COMPUTERNAME"))
(defvar *pj/home-computer-p* (member *pj/computer-name* '("SAMURAI10" "LAPTOP10")))
(defvar *pj/work-computer-p* (member *pj/computer-name* '("L523594")))

(message " *** computer name is: %s" *pj/computer-name*)

(defvar *pj/execution-platform*
  (or (and (functionp 'w32-send-sys-command) 'win32)
      'unix))
(defvar *pj/win32-platform-p* (eq *pj/execution-platform* 'win32))
(defvar *pj/unix-platform-p* (eq *pj/execution-platform* 'unix))

;; ---------------------------------------------------------------------
;; As set-default-font is local to the current frame, it's better to use
;; an .Xdefault entry or a Windows registry entry.  Under windows,
;; create a value in the key HKEY_CURRENT_USER\Software\GNU\Emacs:
;;
;; Value Name: Emacs.Font
;; Value Type: REG_SZ
;; String: -*-Courier-normal-r-*-*-13-*-*-*-c-*-iso8859-1
;; String: -*-DejaVu Sans Mono-normal-r-*-*-13-*-*-*-c-*-iso8859-1
;; ---------------------------------------------------------------------

;;
;; start the server
;;
;; (server-start)
;; deprecated, now user runemacs.exe -t or runemacs.exe -a ' '

;;
;; elisp files location
;;
(setq load-path (cons "~/Mes Outils Personnels/scripts/emacs" load-path))
(setq load-path (cons "~/Mes Outils Personnels/scripts/elisp" load-path))

;;
;; basic customization
;;
(load-library "pj-basic-setup")

;;
;; auto recompile on save
;;
(autoload 'pj/auto-recompile-file-always "pj-auto-recompile-setup"
  "Auto recompile current emacs lisp file on save" t)
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (add-hook 'after-save-hook 'pj/auto-recompile-file-always)))

;;
;; window system dependent parameters
;;
(if (display-graphic-p)
    (progn
      (pj/dot-emacs-msg "WINDOW-ONLY-SETTINGS")

      ;;
      ;; basic window system configuration
      ;;
      (load-library "pj-window-setup")

      ;;
      ;; Close the current frame and not the main one through C-x C-c.
      ;;
      (global-set-key "\C-x\C-c" 'pj/intelligent-close)
      (autoload 'pj/intelligent-close "pj-window-functions" "Frame intelligent close" t)

      ;;
      ;; Firefox-like fullscreen binding
      ;;
      (global-set-key [f11] 'pj/maximize-or-restore)
      (autoload 'pj/maximize-or-restore "pj-window-functions"
        "Maximize to full screen or restore to normal state" t)

      ;;
      ;; C-c m to move frame with keyboard
      ;;
      (global-set-key "\C-cm" 'pj/move-frame)
      (autoload 'pj/move-frame "pj-window-functions" "Move frame" t)

      ;;
      ;; C-c r to resize frame with keyboard
      ;;
      (global-set-key "\C-cr" 'pj/resize-frame)
      (autoload 'pj/resize-frame "pj-window-functions" "Resize frame" t)

      ;;
      ;; win32 font helper
      ;;
      (if *pj/win32-platform-p*
          (autoload 'pj/find-xfld-font-name "pj-window-functions" "XFLD helper" t))

      ;; take any buffer and turn it into an html file, including syntax
      ;; highlighting
      (autoload 'htmlize-buffer "htmlize" "make a html file from the buffer" t)
      (autoload 'htmlize-region "htmlize" "make a html file from a region" t)

      ;;
      ;; Tabbar
      ;;
      ;; (load-library "pj-tabbar-setup")
      ;; not working in emacs 24?

      ;;
      ;; Speedbar activation through f12
      ;;
      (global-set-key [f12] 'pj/speedbar-show-hide)
      (autoload 'pj/speedbar-show-hide "pj-speedbar-setup" "Show/Hide speedbar")

	  ;; load theme
	  (load-theme 'adwaita t)
	)
  (progn
    (pj/dot-emacs-msg "CONSOLE-ONLY-SETTINGS")))


;; (add-to-list 'load-path
;;              "~/Scripts/elisp/ecb")
;; (require 'ecb)
;; (require 'ecb-autoloads)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; GENERAL KEY BINDINGS AND ALIAS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; do scroll horizontally when at edge of screen
(setq auto-hscroll-mode t)
(setq hscroll-margin 0)
(setq hscroll-step 5)

;; slight horizontal scroll through <C-PageUp/C-PageDown>
(defun pj/scroll-right-slightly () (interactive) (scroll-right 5))
(defun pj/scroll-left-slightly () (interactive) (scroll-left 5))
(global-set-key [C-next] 'pj/scroll-right-slightly)
(global-set-key [C-prior] 'pj/scroll-left-slightly)

;; (setq compilation-read-command nil)
(setq compile-command "make -k")
(global-set-key '[f9] 'compile)

(global-set-key [delete] 'delete-char)

(global-set-key "\^c\^c" 'comment-dwim)
(global-set-key "\^x\^h" 'gnus)
(global-set-key [C-tab] 'other-window)

(global-set-key "\C-m" 'newline-and-indent)

;; kill current buffer without confirmation
;; (global-set-key [C-f4] 'kill-current-buffer)
(global-set-key [f3] 'pj/kill-current-buffer)
(defun pj/kill-current-buffer ()
  "Kill the current buffer, without confirmation."
  (interactive)
  (kill-buffer (current-buffer)))

;;
;; Shows yank menu to easily browse the kill-ring
;;
(global-set-key "\M-\C-y"
                '(lambda ()
                   (interactive)
                   (popup-menu 'yank-menu)))

;;
;; sweet alias
;;
(defun pj/emacs () (interactive) (find-file "~/Scripts/emacs/emacs.el"))
(defun pj/codes () (interactive) (find-file "~/Documents/Privacy/codes.txt"))

;;
;; better file loading interactions
;;
(require 'ido)
(ido-mode t)

;;
;; Display line numbers
;;
(autoload 'linum-mode "linum-patched" "Toggle display of line numbers." t)

;;
;; Stretch cursor for tabs
;;
(setq x-stretch-cursor t)

;;
;; buffer cycling
;;
(autoload 'pj/yic-prev-buffer "pj-buffer-setup" "Cycle to previous buffer" t)
(autoload 'pj/yic-next-buffer "pj-buffer-setup" "Cycle to next buffer" t)
;; Go through buffer with Shift-PageUp and Shift-PageDown
(global-set-key [S-next] 'pj/yic-next-buffer)
(global-set-key [S-prior] 'pj/yic-prev-buffer)

;;
;; buffer move through C-S-[up|down|left|right]
;;
(autoload 'buf-move-up "buffer-move" "Move buffer up" t)
(autoload 'buf-move-down "buffer-move" "Move buffer down" t)
(autoload 'buf-move-left "buffer-move" "Move buffer left" t)
(autoload 'buf-move-right "buffer-move" "Move buffer right" t)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)

;;
;; org mode setup
;;
(if *pj/home-computer-p*
    (setq org-agenda-files (list "~/Documents/org/mygtd.org"
                                 "~/Websites/ibusinessshop.com.3.0/todo.org"))
  (setq org-agenda-files (list "~/Documents/org/mygtd.org")))
(add-to-list 'auto-mode-alist '("\\.org\\'" . pj/org-mode))
(autoload 'pj/org-mode "pj-org-setup" "Setup my org mode" t)
(defun pj/gtd () (interactive) (find-file "~/Documents/Org/mygtd.org"))

;;
;; files shortcuts
;;
(defun pj/chsheet () (interactive) (find-file "~/Scripts/emacs/refcards/cheat-sheet.org"))

;;
;; cygwin customization
;;
(defvar *pj/cygwin-path-prefix* "")
(if *pj/work-computer-p*
    (setq *pj/cygwin-path-prefix* "d:/outils/cygwin/")
  (setq *pj/cygwin-path-prefix* "d:/cygwin/"))

(setenv "PATH" (concat *pj/cygwin-path-prefix* "bin;" (getenv "PATH")))
(setq exec-path (cons (concat *pj/cygwin-path-prefix* "bin/") exec-path))
(setq Info-default-directory-list
      (append Info-default-directory-list (list (concat *pj/cygwin-path-prefix* "usr/info/"))))

(autoload 'pj/bash "pj-cygwin-setup" "Start cygwin bash shell" t)
(autoload 'pj/set-shell-bash "pj-cygwin-setup" "Use bash shell" t)
(autoload 'pj/set-shell-cmdproxy "pj-cygwin-setup" "Use cmdproxy shell" t)

;;
;; irc is still fun...
;;
(if *pj/home-computer-p*
    (progn
      (autoload 'pj/erc "pj-erc-setup" "Launch customized erc" t)
      (autoload 'pj/irc "pj-erc-setup" "Connect to my IRC networks" t)))

(autoload 'pj/erc "pj-erc-setup" "Launch customized erc" t)
(autoload 'pj/irc "pj-erc-setup" "Connect to my IRC networks" t)

;;
;; latex stuffs
;;
(add-hook 'latex-mode-hook
          (lambda ()
            (autoload 'pj/tex-to-accents "pj-latex-setup" "Convert latex accents" t)
            (autoload 'pj/accents-to-tex "pj-latex-setup" "Convert accents to latex" t)))


;; c/cpp mode stuff
;;
(add-hook 'c-mode-hook 'pj/c-mode-initialize)
(add-hook 'c++-mode-hook 'pj/c++-mode-initialize)
(add-hook 'c-mode-common-hook 'pj/c-mode-common-initialize)
(autoload 'pj/c-mode-initialize "pj-cmode-setup" "Customize c-mode on startup" t)
(autoload 'pj/c++-mode-initialize "pj-cmode-setup" "Customize c++-mode on startup" t)
(autoload 'pj/c-mode-common-initialize "pj-cmode-setup" "Customize CC Mode derived modes" t)

;;
;; ocaml mode
;;
(add-to-list 'load-path "~/Scripts/elisp/tuareg-mode")
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(autoload 'camldebug "camldebug" "Run the Caml debugger" t)
(autoload 'tuareg-imenu-set-imenu "tuareg-imenu"
  "Configuration of imenu for tuareg" t)

(add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)

(setq auto-mode-alist
      (append '(("\\.ml[ily]?$" . tuareg-mode)
                ("\\.topml$" . tuareg-mode))
              auto-mode-alist))

;;
;; display imenu
;;
(global-set-key [mouse-3] 'imenu)

;;
;; turn on auto fill for text mode
;;
(add-hook 'text-mode-hook
          (function (lambda ()
                      (turn-on-auto-fill))))

;;
;; csv files support
;;
(add-to-list 'auto-mode-alist '("\\.[Cc][Ss][Vv]\\'" . csv-mode))
(autoload 'csv-mode "csv-mode" "Major mode for editing comma-separated value files." t)

;;
;; apache configuration files
;;
(autoload 'apache-mode "apache-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.htaccess\\'"   . apache-mode))
(add-to-list 'auto-mode-alist '("httpd\\.conf\\'"  . apache-mode))
(add-to-list 'auto-mode-alist '("srm\\.conf\\'"    . apache-mode))
(add-to-list 'auto-mode-alist '("access\\.conf\\'" . apache-mode))
(add-to-list 'auto-mode-alist '("sites-\\(available\\|enabled\\)/" . apache-mode))

;;
;; norton commander like shell
;;
(autoload 'nc "nc" "Emulate NC file shell" t)

;;
;; web development setup (OBSOLETE)
;;
; (load-library "pj-webdev-setup")

;;
;; dired/image-dired customization
;;
(defvar *pj/image-magick-prefix* "")
(if *pj/work-computer-p*
    (setq *pj/image-magick-prefix* "d:/local/outils/ImageMagick/bin/")
  (if *pj/home-computer-p*
      (setq *pj/image-magick-prefix* "c:/Program Files/ImageMagic-6.3.9-Q8/")))
(load-library "pj-dired-setup")

;;
;; dired recycler
;;
;; (require 'trashcan)

;;
;; completion through expand
;;
(autoload 'pj/indent-or-expand "pj-expand-setup" "Improved indent/expand" t)
(defun pj/tab-fix ()
  (local-set-key [tab] 'pj/indent-or-expand))
(add-hook 'c-mode-hook          'pj/tab-fix)
;(add-hook 'sh-mode-hook         'pj/tab-fix)
(add-hook 'emacs-lisp-mode-hook 'pj/tab-fix)
(add-hook 'php-mode-hook        'pj/tab-fix)
(add-hook 'java-mode-hook       'pj/tab-fix)


;;
;; include misc functions
;;
(autoload 'pj/unix2dos "pj-misc-functions" "Convert UNIX buffer to DOS buffer" t)
(autoload 'pj/dos2unix "pj-misc-functions" "Convert DOS buffer to UNIX buffer" t)
(autoload 'pj/indent-whole-buffer "pj-misc-functions" "Indent the current buffer" t)
(autoload 'pj/rename-file-and-buffer "pj-misc-functions" "Rename both current buffer and associated file" t)
(autoload 'pj/move-buffer-file "pj-misc-functions" "Move file associated to the current buffer" t)
(autoload 'pj/swap-windows "pj-misc-functions" "Swap two windows" t)
(autoload 'pj/ascii-table "pj-misc-functions" "Display ASCII table" t)
(autoload 'pj/toggle-selective-display "pj-misc-functions" "Hide/show first code block" t)
(global-set-key [f1] 'pj/toggle-selective-display)

;;
;; use my gnus setup (OBSOLETE)
;;
; (setq gnus-init-file "~/Scripts/emacs/pj-gnus-setup")

;;
;; calendar
;;
(eval-when-compile (require 'calendar))

;; week begin on monday
(setq calendar-week-start-day 1)

;; month/day/year date format
(setq calendar-date-style 'european)

;; christian holydays
(setq all-christian-calendar-holiday t)

;; french translation
;; (defvar calendar-day-abbrev-array
;;   ["dim" "lun" "mar" "mer" "jeu" "ven" "sam"])
;; (defvar calendar-day-name-array
;;   ["dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi"])
;; (defvar calendar-month-abbrev-array
;;   ["jan" "f�v" "mar" "avr" "mai" "jun"
;;    "jul" "ao�" "sep" "oct" "nov" "d�c"])
;; (defvar calendar-month-name-array
;;   ["janvier" "f�vrier" "mars" "avril" "mai" "juin"
;;    "juillet" "ao�t" "septembre" "octobre" "novembre" "d�cembre"])


;;
;; now it's time to type "M-x tetris"
;;
(message " ")
(message "===========[ end-of-EMACS ]===========")
(message " ")

;;
;; Credits:
;; --------
;;
;; I may have forget some people over the years.
;;
;;  * pj/dot-emacs-msg inspired by
;;    http://ourcomments.org/Emacs/DL/elisp/dot-emacs/.emacs-real.html
;;
;;  * some corrections in 2015 thanks to
;;    http://a-nickels-worth.blogspot.fr/2007/11/effective-emacs.html
;;
;;  * loading time corrected from
;;	   http://www.emacswiki.org/emacs/OptimizingEmacsStartup
