(pj/dot-emacs-msg "start-of-ERC-SETUP")

(eval-when-compile
  (require 'mpg123)
  (require 'erc)
  (require 'erc-join)
  (require 'erc-track)
  (require 'erc-stamp)
  (require 'erc-fill)
  (require 'erc-log)
  (require 'erc-truncate)
  (require 'erc-match))

(defun pj/erc ()

  (require 'erc)

  ;; Add extras to load-path
  ;;(add-to-list 'load-path "~/Scripts/elisp/erc-5.2-extras")

  ;; popup query buffer in case of private message
  (setq erc-auto-query 'buffer)

  ;; better for international chats...
  (setq erc-server-coding-system 'utf-8)

  ;; exclude some messages from channel tracking
  (setq erc-track-exclude-types '("PART" "QUIT" "JOIN" "MODE"))

  ;; timestamp + nice filling
  (setq erc-timestamp-only-if-changed-flag nil
        erc-timestamp-format "[%H:%M] "
        erc-fill-prefix "      + "
        erc-insert-timestamp-function 'erc-insert-timestamp-left)

  ;; Disable auto-fill for erc buffers
  (add-hook 'erc-mode-hook
            (lambda ()
              (auto-fill-mode 0)
              (setq show-trailing-whitespace nil)))

  ;; Show nick list through erc-nicklist
  (require 'erc-nicklist)

  ;; Hilight on keywords
  (require 'erc-match)
  (erc-match-mode 1)

  ;; Keybinding C-c C-q to start a query
  (define-key erc-mode-map (kbd "C-c C-q")
    (lambda (nick)
      (interactive (list (completing-read "Nick: " erc-channel-users)))
      (erc-cmd-QUERY nick)))

  ;; Add an underscore if the nick is already in use
  (setq erc-nick-uniquifier "_")

  ;; Kill buffers for channels after /part
  (setq erc-kill-buffer-on-part t)

  ;; Kill buffers for server messages after quitting the server
  (setq erc-kill-server-buffer-on-quit t)

  ;; automatically truncate humongous buffers
  (setq erc-max-buffer-size 30000)
  (defvar erc-insert-post-hook)
  (add-hook 'erc-insert-post-hook
            'erc-truncate-buffer)
  (setq erc-truncate-buffer-on-save t)

  ;; activate nicknames coloration
  (and
   (require 'erc-highlight-nicknames)
   (add-to-list 'erc-modules 'highlight-nicknames)
   (erc-update-modules))

  ;; shows the number of opped/voiced/normal members of the current
  ;; channel in the modeline
  ;; (define-minor-mode ncm-mode "" nil
  ;;   (:eval
  ;;    (let ((ops 0)
  ;;         (voices 0)
  ;;         (members 0))
  ;;     (maphash (lambda (key value)
  ;;                (when (erc-channel-user-op-p key)
  ;;                  (setq ops (1+ ops)))
  ;;                (when (erc-channel-user-voice-p key)
  ;;                  (setq voices (1+ voices)))
  ;;                (setq members (1+ members)))
  ;;              erc-channel-users)
  ;;     (format " %S/%S/%S" ops voices members))))

  ;; shows "Now playing: ..."
  (defun erc-cmd-PLAYING (&rest ignore)
    "Post what mpg123 is playing."
    (interactive)
    (when (mpg123-now-playing)
      (erc-send-message (concat "Now playing: " (mpg123-now-playing)))))

  ;; shows uname
  (defun erc-cmd-UNAME (&rest ignore)
    "Display the result of running `uname -a' to the current ERC buffer."
    (let ((uname-output
           (replace-regexp-in-string
            "[ \n]+$" "" (shell-command-to-string "uname -a"))))
      (erc-send-message
       (concat "{uname -a} [" uname-output "]"))))

  (defun erc-cmd-EMACSVERSION (&rest ignore)
    "Post emacs version"
    (erc-send-message
     (concat "(emacs-version) [" (emacs-version) "]")))


  ;; irc doctor
  (setq erc-remove-parsed-property nil)

  (autoload 'doctor-doc "doctor")
  (autoload 'make-doctor-variables "doctor")

  (defvar erc-doctor-id "{Emacs doctor} ")

  (defun erc-cmd-DOCTOR (&optional last-sender &rest ignore)
    "Get the last message in the channel and doctor it."
    (let ((limit (- (point) 1000))
          (pos (point))
          data
          doctor-buffer
          last-message
          text)
      ;; Make sure limit is not negative
      (when (< limit 0) (setq limit 0))
      ;; Search backwards for text from someone
      (while (and pos
                  (not (and (setq data
                                  (get-text-property pos 'erc-parsed))
                            (string= (aref data 3) "PRIVMSG")
                            (or (not last-sender)
                                (string= (car (split-string
                                               (aref data 2) "!"))
                                         last-sender)))))
        (setq pos (previous-single-property-change
                   pos 'erc-parsed nil limit))
        (when (= pos limit)
          (error "No appropriate previous message to doctor")))
      (when pos
        (setq last-sender (car (split-string
                                (aref (get-text-property
                                       pos 'erc-parsed) 2) "!"))
              doctor-buffer (concat "*ERC Doctor: " last-sender "*")
              last-message (split-string
                            ;; Remove punctuation from end of sentence
                            (replace-regexp-in-string
                             "[ .?!;,/]+$" ""
                             (aref (get-text-property pos
                                                      'erc-parsed) 5)))
              text (mapcar (lambda (s)
                             (intern (downcase s)))
                           ;; Remove salutation if it exists
                           (if (string-match
                                (concat "^" erc-valid-nick-regexp
                                        "[:,]*$\\|[:,]+$")
                                (car last-message))
                               (cdr last-message)
                             last-message))))
      (erc-send-message
       (concat erc-doctor-id
               ;; Only display sender if not in a query buffer
               (if (not (erc-query-buffer-p))
                   (concat last-sender ": "))
               (save-excursion
                 (if (get-buffer doctor-buffer)
                     (set-buffer doctor-buffer)
                   (set-buffer (get-buffer-create doctor-buffer))
                   (make-doctor-variables))
                 (erase-buffer)
                 (doctor-doc text)
                 (buffer-string))))))
  )

;; sends a message to all active channels so that I'm able to type:
;;     /amsg Hi, everyone!  Good morning :-)
(defun erc-cmd-AMSG (&rest args)
      "Send a message to all the channels of the current server."
      (when args
        (let ((message (substring (mapconcat #'identity args " ") 1))
          (channel-list (and erc-server-process
                             (erc-channel-list erc-server-process))))
          (when channel-list
            (dolist (channel channel-list)
              (with-current-buffer channel
                (erc-send-message message)))))))

;; Finally, connect to irc network(s)
(defun pj/irc ()
  "Connect to IRC network(s)"
  (interactive)
  (when (y-or-n-p "irc ? ")
    (pj/erc)                            ; initialize erc environment
    (setq erc-autojoin-channels-alist   ; set auto join channels
          '(("nerim.fr" "#convivial" "#demofr" "#thescene" "#illusionirc" "#mds" "#freebsd")
            ("freenode.net" "#emacs" "#codeigniter" "#lisp" "#lispcafe" "#cl-gardeners")))
    (erc :server "irc.nerim.fr"
         :port 6667
         :nick "babyloon"
         :full-name "bbl^rvl"
         :password nil)
    (erc :server "irc.freenode.net"
         :port 6667
         :nick "pjfr33"
         :full-name "Pierre-Jean"
         :password nil)))

(pj/dot-emacs-msg "end-of-ERC-SETUP")

;;
;; Credits:
;; --------
;;
;; I may have forget some people over the years.
;;
;;  * /uname and /doctor commands have ripped from MichaelOlson
;;    http://mwolson.org/projects/emacs-config/erc-init.el.html
;;
