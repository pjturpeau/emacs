
(pj/dot-emacs-msg "start-of-MISC-FUNCTIONS")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; UNIX <=> DOS CRLF CONVERSION
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/dos2unix ()
  (interactive)
  (goto-char (point-min))
  (while (search-forward "\r" nil t) (replace-match "")))

(defun pj/unix2dos ()
  (interactive)
  (goto-char (point-min))
  (while (search-forward "\n" nil t) (replace-match "\r\n")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; INDENT WHOLE BUFFER
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/indent-whole-buffer ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; RENAME CURRENT BUFFER AND FILE
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file name new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; MOVE BOTH BUFFER AND FILE
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/move-buffer-file (dir)
  "Moves both current buffer and file it's visiting to DIR."
  (interactive "DNew directory: ")
  (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (dir
          (if (string-match dir "\\(?:/\\|\\\\)$")
              (substring dir 0 -1) dir))
         (newname (concat dir "/" name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (progn
        (copy-file filename newname 1)
        (delete-file filename)
        (set-visited-file-name newname)
        (set-buffer-modified-p nil)
        t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; rotate two windows
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/swap-windows ()
  "If you have 2 windows, it swaps them."
  (interactive)
  (cond ((not (= (count-windows) 2))
         (message "You need exactly 2 windows to do this."))
        (t
         (let* ((w1 (first (window-list)))
                (w2 (second (window-list)))
                (b1 (window-buffer w1))
                (b2 (window-buffer w2))
                (s1 (window-start w1))
                (s2 (window-start w2)))
           (set-window-buffer w1 b2)
           (set-window-buffer w2 b1)
           (set-window-start w1 s2)
           (set-window-start w2 s1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; hide/show level 1 blocks
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/toggle-selective-display (column)
 (interactive "P")
 (set-selective-display
  (if selective-display nil (or column 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; show ascii table
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/ascii-table ()
  "Print an ASCII table. Based on a defun by Alex Schroeder <asc@bsiag.com>"
  (interactive)  (switch-to-buffer "*ASCII*")  (erase-buffer)
  (insert (format "ASCII characters up to number %d.\n" 255))
    (let ((i 0))
      (while (<= i 255)
        (insert (format "%4d | o%03o | 0x%02x | %c\n" i i i i))
        (setq i (+ i 1))
      ))
  (beginning-of-buffer))

(pj/dot-emacs-msg "end-of-MISC-FUNCTIONS")

;;
;; Credits:
;; --------
;;
;; I may have forgot some people over the years.
;;
;;  * pj/swap-windows, pj/rename-file-and-buffer and pj/move-buffer-file
;;    are simply ripped as found in SteveyDrunken's emacs file
;;    http://steve.yegge.googlepages.com/my-dot-emacs-file
;;
;;  * pj/indent-whole-buffer is ripped from RobChristie's blog
;;    http://www.emacsblog.org/2007/01/17/indent-whole-buffer/
;;
;;  * pj/dos2unix and pj/unix2dos are from BenjaminRutt's emacs file
;;    http://www.dotemacs.de/dotfiles/BenjaminRutt.emacs.html
;;
;;  * pj/toggle-selective-display is a pure copy/past from
;;    JoséAntonioOrtegaRuiz
;;    http://emacs.wordpress.com/2007/01/16/quick-and-dirty-code-folding/
;;