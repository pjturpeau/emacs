(pj/dot-emacs-msg "start-of-CMODE-SETUP")

(defun pj/c-mode-initialize ()
  (define-key c-mode-map "\^x\^e" 'compile) ;useless? F9 binding enough ?
  (local-set-key "\^c\^i" 'pj/c-insert-header-ifdef)
  (c-toggle-auto-newline)
  (c-set-style "BSD")
  (setq tab-width 4)
  (setq c-basic-offset 4)

  ;; highlight function calls in code
  (font-lock-add-keywords
   nil
   '(("\\<\\(\\sw+\\) ?(" 1 font-lock-function-name-face)) t))

(defun pj/c++-mode-initialize ()
  ;; highlight function calls in code
  (font-lock-add-keywords
   nil
   '(("\\<\\(\\sw+\\) ?(" 1 font-lock-function-name-face)) t))


(defun pj/c-mode-common-initialize ()
  (auto-fill-mode 1)

  ;; automatically fill comments but not code
  (set (make-local-variable 'fill-nobreak-predicate)
       (lambda ()
         (not (eq (get-text-property (point) 'face)
                  'font-lock-comment-face)))))


(defun pj/c-insert-header-ifdef ()
  "Insert #ifdef directive in a .h file"
  (interactive)
  (let* ((name (file-name-nondirectory buffer-file-name))
         (cpp-name (format "%s_H" (upcase (substring name 0 -2))))
         (suffix (substring name -2)))
    (if (string-equal suffix ".h")
        (save-excursion
          (goto-char (point-min))
          (let ((first-line (save-excursion
                              (end-of-line 1)
                              (buffer-substring (point-min) (point))))
                (pattern (format "^#ifndef %s" cpp-name)))
            (if (string-match pattern first-line)
                (error "First line is \"%s%s\": no change." pattern "...")))
          (insert (format "#ifndef %s\n" cpp-name))
          (insert (format "#define %s\n\n" cpp-name))
          (goto-char (point-max))
          (insert (format "\n#endif /* %s */\n" cpp-name)))
      (error "Current file must have .h suffix"))))

(defun pj/c++-insert-header-ifdef ()
  "Insert #ifdef directive in a .hh file"
  (interactive)
  (let* ((name (file-name-nondirectory buffer-file-name))
         (cpp-name (format "%s_HH" (upcase (substring name 0 -3))))
         (suffix (substring name -3)))
    (if (string-equal suffix ".hh")
        (save-excursion
          (goto-char (point-min))
          (let ((first-line (save-excursion
                              (end-of-line 1)
                              (buffer-substring (point-min) (point))))
                (pattern (format "^#if !defined (%s)" cpp-name)))
            (if (string-match pattern first-line)
                (error "First line is \"%s%s\": no change." pattern "...")))
          (insert (format "#if !defined (%s)\n" cpp-name))
          (insert (format "#define %s\n\n" cpp-name))
          (goto-char (point-max))
          (insert (format "\n#endif /* %s */\n" cpp-name)))
      (error "Current file must have .hh suffix"))))

(defun pj/c-for-i (val_max)
  "for loop with i"
  (interactive "sval_max:")
  (insert "for (i = 0; i < ")
  (insert val_max)
  (insert "; i++)\n{")
  (c-indent-command)
  (open-line 2)
  (next-line 2)
  (insert "}")
  (c-indent-command)
  (previous-line 1)
  (c-indent-command))

(defun pj/c-for-j (val_max)
  "for loop with j"
  (interactive "sval_max:")
  (insert "for (j = 0; j < ")
  (insert val_max)
  (insert "; j++)\n{")
  (c-indent-command)
  (open-line 2)
  (next-line 2)
  (insert "}")
  (c-indent-command)
  (previous-line 1)
  (c-indent-command))

(defun pj/c-printf (format params)
  "printf command"
  (interactive "sformat: \nsparams: ")
  (insert "printf (\"")
  (insert format)
  (if (equal params "" )
      (insert "\\n\"" )
    (insert "\\n\", " params))
  (insert "\);")
  (c-indent-command)
  (open-line 1)
  (next-line 1)
  (c-indent-command))

(defun pj/c-fprintf (format params)
  "fprintf command"
  (interactive "sformat: \nsparams: ")
  (insert "fprintf (stderr, \"")
  (insert format)
  (if (equal params "" )
      (insert "\\n\"" )
    (insert "\\n\", " params))
  (insert "\);")
  (c-indent-command)
  (open-line 1)
  (next-line 1)
  (c-indent-command))

(pj/dot-emacs-msg "end-of-CMODE-SETUP")
