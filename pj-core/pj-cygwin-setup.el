;;; Derived from http://www.emacswiki.org/cgi-bin/emacs/setup-cygwin.el
;;; with modifications regarding to cygwin installation

(pj/dot-emacs-msg "start-of-CYGWIN-SETUP")

;;; Follow Cygwin symlinks.
;;; Handles old-style (text file) symlinks and new-style (.lnk file) symlinks.
;;; (Non-Cygwin-symlink .lnk files, such as desktop shortcuts, are still loaded as such.)
(defun pj/follow-cygwin-symlink ()
  "Follow Cygwin symlinks.
Handles old-style (text file) and new-style (.lnk file) symlinks.
\(Non-Cygwin-symlink .lnk files, such as desktop shortcuts, are still
loaded as such.)"
  (save-excursion
    (goto-char 0)
    (if (looking-at
         "L\x000\x000\x000\x001\x014\x002\x000\x000\x000\x000\x000\x0C0\x000\x000\x000\x000\x000\x000\x046\x00C")
        (progn
          (re-search-forward
           "\x000\\([-A-Za-z0-9_\\.\\\\\\$%@(){}~!#^'`][-A-Za-z0-9_\\.\\\\\\$%@(){}~!#^'`]+\\)")
          (find-alternate-file (match-string 1)))
      (if (looking-at "!<symlink>")
          (progn
            (re-search-forward "!<symlink>\\(.*\\)\0")
            (find-alternate-file (match-string 1))))
      )))

(add-hook 'find-file-hooks 'pj/follow-cygwin-symlink)

;;; Use Unix-style line endings.
(setq-default buffer-file-coding-system 'undecided-unix)

;; shell should handle colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; Use unix coding system
(add-hook 'shell-mode-hook
          (lambda ()
            (set-process-coding-system
             (get-buffer-process (current-buffer)) 'unix 'unix)))

;; (setenv "PATH" (concat pj/cygwin-path-prefix "bin;" (getenv "PATH")))
;; (setq exec-path (cons (concat pj/cygwin-path-prefix "bin/") exec-path))
;; (setq Info-default-directory-list
;;       (append Info-default-directory-list (list (concat pj/cygwin-path-prefix "usr/info/"))))
(setq shell-file-name "bash.exe")

;; Replaces slashes by double anti-slashes to be able to set path with
;; the following form: "d:\\cygwin\\bin". However, this is a bit useless
;; since the standard seems to work perfectly (but I've seen this
;; approach in other .emacs files...)
;; (let ((string-to-replace pj/cygwin-path-prefix))
;;   (while (string-match "/" string-to-replace)
;;     (setq string-to-replace (replace-match "\\" nil t string-to-replace)))
;;   (setenv "PATH" (concat (getenv "PATH") (concat ";" string-to-replace "bin"))))

;;
;; make cygwin paths accessible
;;
(require 'cygwin-mount)
(cygwin-mount-activate)

(setenv "SHELL" shell-file-name)

(setq explicit-shell-file-name shell-file-name) ; Interactive shell
(setq explicit-bash-args '("-login" "-i"))
(setq w32-quote-process-args ?\") ;; " @@@ IS THIS BETTER? ;@@@ WAS THIS BEFORE: (setq w32-quote-process-args t)

;; strip ^M chars
(add-hook 'comint-output-filter-functions
          'shell-strip-ctrl-m nil t)

;; filter passwords
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt nil t)

(defun pj/bash ()
  "Start `bash' shell."
  (interactive)
  (pj/set-shell-bash)
  (let ((binary-process-input t)
        (binary-process-output nil))
    (shell)))

(setq process-coding-system-alist
      (cons '("bash" . (raw-text-dos . raw-text-unix)) process-coding-system-alist))

;; From: http://www.dotfiles.com/files/6/235_.emacs
(defun pj/set-shell-bash()
  "Enable on-the-fly switching between the bash shell and DOS."
  (interactive)
  ;; (setq binary-process-input t)
  (setq shell-file-name "bash")
  (setq shell-command-switch "-c")      ; SHOULD IT BE (setq shell-command-switch "-ic")?
  (setq explicit-shell-file-name "bash")
  (setenv "SHELL" explicit-shell-file-name)
  (setq explicit-bash-args '("-login" "-i"))
  (setq w32-quote-process-args ?\") ;; "
  )

(defun pj/set-shell-cmdproxy()
  "Set shell to `cmdproxy'."
  (interactive)
  (setq shell-file-name "cmdproxy")
  (setq explicit-shell-file-name "cmdproxy")
  (setenv "SHELL" explicit-shell-file-name)
  ;;;;;(setq explicit-sh-args nil)           ; Undefined?
  (setq w32-quote-process-args t))

;; ??? how to do the same thing in shell ????
;; (defun eshell/op (file)
;;   (w32-shell-execute "Open" (substitute ?\\ ?/ (expand-file-name file))) nil)

;;;;;;;;;;;;;;;;;;;;;;;

(provide 'cygwin-setup)

(pj/dot-emacs-msg "end-of-CYGWIN-SETUP>")