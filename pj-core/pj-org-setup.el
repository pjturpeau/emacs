(pj/dot-emacs-msg "start-of-ORG-SETUP")

;; use native org mode...
;; (add-to-list 'load-path "~/Archives/Config/elisp/org")

(eval-when-compile (require 'org))

(defvar *pj/org-mode-initialized* nil)

(defun pj/org-mode ()
  (if (not *pj/org-mode-initialized*)
      (progn
        (require 'org-install)
        (add-to-list 'auto-mode-alist '("\\.org\\'" . pj/org-mode))

        ;;(global-set-key "\C-cl" 'org-store-link)
        (global-set-key "\C-ca" 'org-agenda)

        ;; do not fold items by default
        (setq org-startup-folded 'showall)

        (add-hook 'org-mode-hook
                  (lambda ()
                    (require 'org-mouse)

                    ;; remove leading stars
                    (setq org-hide-leading-stars t)

                    ;; follows links when hitting <RET> key
                    (setq org-return-follows-link t)

                    ;; use only odd levels to have a cleaner headlines separation
                    (setq org-odd-levels-only t)

                    ;; keep track of *when* a certain TODO was finished
                    (setq org-log-done 'time)

                    ;; add a note to closed TODO
                    (setq org-log-done 'note)))
        (setq *pj/org-mode-initialized* t)))
  (org-mode))

;; remove comments from org document for use with export hook
;; https://emacs.stackexchange.com/questions/22574/orgmode-export-how-to-prevent-a-new-line-for-comment-lines
;; (defun pj/org-delete-org-comments (backend)
;;   (loop for comment in (reverse (org-element-map (org-element-parse-buffer)
;;                                     'comment 'identity))
;;         do
;;         (setf (buffer-substring (org-element-property :begin comment)
;;                                 (org-element-property :end comment))
;;               "")))

;; add to export hook
;; (add-hook 'org-export-before-processing-hook 'pj/org-delete-org-comments)

;;
;; * pj/org-delete-org-comments by http://pragmaticemacs.com/
;;

(pj/dot-emacs-msg "end-of-ORG-SETUP")
