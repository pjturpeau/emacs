(pj/dot-emacs-msg "start-of-BUFFER-SETUP")

(defun pj/yic-ignore (str)
  (or
   ;;buffers I don't want to switch to
   (string-match "\\*Buffer List\\*" str)
   (string-match "^\\*scratch\\*$" str)
   (string-match "\\*Backtrace\\*" str)
   (string-match "\\*Help\\*" str)
   (string-match "\\*Compile-Log\\*" str)
   (string-match "\\*Finder\\*" str)
   (string-match "^TAGS" str)
   (string-match "^\\*Messages\\*$" str)
   (string-match "^\\*Completions\\*$" str)
   (string-match "^ " str)

   ;;Test to see if the window is visible on an existing visible frame.
   ;;Because I can always ALT-TAB to that visible frame, I never want to
   ;;Ctrl-TAB to that buffer in the current frame.  That would cause
   ;;a duplicate top-level buffer inside two frames.
   (memq str
         (mapcar
          (lambda (x)
            (buffer-name
             (window-buffer
              (frame-selected-window x))))
          (visible-frame-list)))
   ))

(defun pj/yic-next (ls)
  "Switch to next buffer in ls skipping unwanted ones."
  (let* ((ptr ls)
         bf bn go)
    (while (and ptr (null go))
      (setq bf (car ptr)  bn (buffer-name bf))
      (if (null (pj/yic-ignore bn))        ;skip over
          (setq go bf)
        (setq ptr (cdr ptr))))
    (if go
        (switch-to-buffer go))))

(defun pj/yic-next-buffer ()
  "Switch to previous buffer in current window."
  (interactive)
  (pj/yic-next (reverse (buffer-list))))

(defun pj/yic-prev-buffer ()
  "Switch to the other buffer (2nd in list-buffer) in current window."
  (interactive)
  (bury-buffer (current-buffer))
  (pj/yic-next (buffer-list)))

(pj/dot-emacs-msg "end-of-MISC-FUNCTIONS")

;;
;; Credits:
;; --------
;;
;; I may have forgot some people over the years.
;;
;;  * switching buffers original code from yic-buffer.el from
;;    choo@cs.yale.edu (young-il choo) and ripped from BenjaminRutt's
;;    http://www.dotemacs.de/dotfiles/BenjaminRutt.emacs.html
;;
