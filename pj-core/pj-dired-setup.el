(pj/dot-emacs-msg "start-of-DIRED-SETUP")

(setq image-dired-cmd-create-thumbnail-program
      (concat *pj/image-magick-prefix* "convert.exe"))
(setq image-dired-cmd-create-temp-image-program
      (concat *pj/image-magick-prefix* "convert.exe"))
(setq image-dired-cmd-rotate-thumbnail-program
      (concat *pj/image-magick-prefix* "mogrify.exe"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; OPEN FILE IN WINDOWS WITH F5
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pj/w32-browser (file)
  "Run default Windows application associated with FILE.
If no associated application, then `find-file' FILE."
  (or (condition-case nil
          (w32-shell-execute nil file)  ; Use Windows file association
        (error nil))
      (find-file file)))              ; E.g. no Windows file association

(eval-after-load "dired"
  '(define-key dired-mode-map [f5]
     (lambda ()
       (interactive)
       (pj/w32-browser (dired-replace-in-string
                        "/" "\\" (dired-get-filename))))))

(pj/dot-emacs-msg "end-of-DIRED-SETUP")
