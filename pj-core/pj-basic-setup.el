(pj/dot-emacs-msg "start-of-BASIC-SETUP")

;; disable toolbar, scrollbars and menu bar
(tool-bar-mode -1)
(scroll-bar-mode t)                    ; <- not the only place...
(menu-bar-mode t)

;; customize display parameters
(setq column-number-mode t)
(setq line-number-mode t)
(setq display-battery-mode t)
(setq size-indication-mode t)

;; remove the annoying bell
(setq visible-bell t)

;; no blinking cursor
(blink-cursor-mode 0)

;; remove splash screen
(setq inhibit-startup-message t)

;; no initial scratch message
(setq initial-scratch-message nil)

;; type y instead of yes
(fset 'yes-or-no-p 'y-or-n-p)

;;; Run the debugger if Emacs detects an error during an Emacs-Lisp
;;; instruction execution.
(setq debug-on-error nil)

;; enable the erase-buffer function
(put 'erase-buffer 'disabled nil)

;; do not write backup files
(setq make-backup-files nil)

;; delete temporary files (#filename#)
(setq delete-auto-save-files t)

;; no empty line at end-of-file
(setq-default next-line-add-newlines nil)

;; indent tab with 4 true spaces only
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

;; use 4 chars spaced tab columns (indent.el)
(setq tab-stop-list '(4 8 12 16 20 24 32 36 40 44 48 52 56 60 64 68 72 76 80 84 88 92 96 100 104 108 112 116))

;; truncate long lines
(setq-default truncate-lines t)

;; Replace highlighted/marked areas
(delete-selection-mode t)

;; Set default column value for auto fill
(setq-default fill-column 79)
(setq-default emacs-lisp-docstring-fill-column 79)

;; allow the mini buffer to resize
(setq resize-mini-windows t)

;; highligh parenthesis match
(show-paren-mode t)

;; show trailing whitespaces in all mode
(setq-default show-trailing-whitespace nil)

;; remove trailing whitespace visibility in several modes
(add-hook 'c-mode-common-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'org-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'emacs-lisp-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'shell-script-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'php-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'html-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'javascript-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'css-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'latex-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'text-mode-hook (lambda () (setq show-trailing-whitespace t)))

;; delete trailing spaces before saving file
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; set time display to 24hr format
(setq display-time-24hr-format t)

;; customize time display
(setq display-time-string-forms
      '((format "%s/%s/%s-%s:%s" day month year 24-hours minutes )
        (if mail "==Mail==" load)))
(display-time)

(if *pj/win32-platform-p*
    (progn
      ;; quote args
      (setq w32-quote-process-args t)))

;; don't waste emerge time by displaying differences in whitespaces
(setq emerge-diff-options "--ignore-all-space")

;; size of compilation window
(setq compilation-window-height 8)

;; Non-nil to scroll the *compilation* buffer window as output appears
(setq compilation-scroll-output t)

;; ;; auto hide compilation window
;; (setq compilation-finish-function       ; DEPRECATED AS OF EMACS 22 !!!
;;       (lambda (buf str)
;;         (if (string-match "exited abnormally" str)
;;             ;;there were errors
;;             (message "compilation errors, press C-x ` to visit")
;;           ;;no errors, make the compilation window go away in 0.5 seconds
;;           (run-at-time 0.5 nil 'delete-windows-on buf)
;;           (message "NO COMPILATION ERRORS!"))))

;; do not create a new frame for woman
(setq woman-use-own-frame nil)

;; Handle .gz files
(auto-compression-mode t)

;; select text with shift + up/down
;(custom-set-variables '(pc-selection-mode t nil (pc-select)))

;; automatically indent pasted lines
(defadvice yank (after indent-region activate)
 (if (member major-mode '(emacs-lisp-mode scheme-mode lisp-mode
                                          c-mode c++-mode objc-mode
                                          latex-mode plain-tex-mode))
     (let ((mark-even-if-inactive t))
       (indent-region (region-beginning) (region-end) nil))))

(defadvice yank-pop (after indent-region activate)
 (if (member major-mode '(emacs-lisp-mode scheme-mode lisp-mode
                                          c-mode c++-mode objc-mode
                                          latex-mode plain-tex-mode))
     (let ((mark-even-if-inactive t))
       (indent-region (region-beginning) (region-end) nil))))



;;
;; improved mode line
;;
(add-to-list 'load-path "~/Scripts/elisp/telephone-line")
(require 'telephone-line)
(telephone-line-mode t)


(pj/dot-emacs-msg "end-of-BASIC-SETUP")

;;
;; Credits:
;; --------
;;
;; I may have forget some people over the years.
;;
;;  * Auto indent on yank
;;    http://www.emacswiki.org/cgi-bin/wiki/AutoIndentation
;;
