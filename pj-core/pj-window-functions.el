(pj/dot-emacs-msg "start-of-WINDOW-FUNCTIONS")

;;
;; Close the current frame and not necessarily the main one
;;
(defun pj/intelligent-close ()
  "quit a frame the same way no matter what kind of frame you are on"
  (interactive)
  (if (eq (car (visible-frame-list)) (selected-frame))
      ;; for parent/master frame...
      (if (> (length (visible-frame-list)) 1)
          ;; close a parent with children present
          (delete-frame (selected-frame))
        ;; close a parent with no children present
        (save-buffers-kill-emacs))
    ;; close a child frame
    (delete-frame (selected-frame))))

;;
;; Fullscreen/restore
;;
(defvar *pj/maximize-or-restore-maximized* 'nil
  "Current maximization state of the frame")

(defun pj/maximize-or-restore ()
  "Maximize to full screen or restore to normal state"
  (interactive)
  (if *pj/win32-platform-p*
      (if *pj/maximize-or-restore-maximized*
          (w32-send-sys-command ?\x0F120)
        (w32-send-sys-command ?\x0F030))
    (message "pj/maximize-or-restore not implemented on this platform"))
  (setq *pj/maximize-or-restore-maximized* (not *pj/maximize-or-restore-maximized*)))

;;
;; Move frame from keyboard
;;
(defun pj/move-frame ()
  "Move frame window"
  (interactive)
  (if *pj/win32-platform-p*
      (w32-send-sys-command ?\x0F010)
    (message "pj/move-frame not implemented on this platform")))

;;
;; Resize frame from keyboard
;;
(defun pj/resize-frame ()
  "Move frame window"
  (interactive)
  (if *pj/win32-platform-p*
      (w32-send-sys-command ?\x0F000)
    (message "pj/resize-frame not implemented on this platform")))

;;
;; XFLD definition helper
;;
(defun pj/find-xfld-font-name ()
  "Find XFLD font name"
  (interactive)
  (insert (prin1-to-string (w32-select-font nil t))))

(pj/dot-emacs-msg "end-of-WINDOW-FUNCTIONS")

;;
;; Credits:
;; --------
;;
;; I may have forgot some people over the years.
;;
;;  * pj/intelligent-close ripped from BenjaminRutt's emacs files
;;    http://www.dotemacs.de/dotfiles/BenjaminRutt.emacs.html
;;