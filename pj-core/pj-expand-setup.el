(pj/dot-emacs-msg "start-of-EXPAND-SETUP")

;; Use hippie expand to improve completion
(require 'hippie-exp)

;; setup hippid expand to maximum completion capability
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-file-name-partially
        try-complete-file-name
        try-expand-list
        try-complete-lisp-symbol-partially
        try-complete-lisp-symbol
        try-expand-whole-kill))

;; bind the expand to the C-TAB combination
;; (global-set-key (quote [(control tab)]) 'hippie-expand)

;;
;; Completion function
;;
(defun pj/indent-or-expand (arg)
  "Try indent function and then completion if it didn't indent anything"
  (interactive "*P")
  (let ((p (point)))
    (indent-according-to-mode)
    (when (and (= p (point))
               (not (bolp))
               (looking-at "\\_>"))
      (hippie-expand arg))))

(pj/dot-emacs-msg "end-of-EXPAND-SETUP")

;;
;; Credits:
;; --------
;;
;; I may have forgot some people over the years.
;;
;;  * pj/indent-or-expand ripped from sean's comment on
;;    http://www.emacsblog.org/2007/03/12/tab-completion-everywhere/
;;