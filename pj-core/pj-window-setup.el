(pj/dot-emacs-msg "start-of-WINDOW-SETUP")

;; inhibit C-z iconify binding...
(global-unset-key "\C-z")

;;
;; set frame properties
;;
(if (display-graphic-p)
    (progn

      (if (> (x-display-pixel-width) 1280)
          (add-to-list 'default-frame-alist (cons 'width 120))
        (add-to-list 'default-frame-alist (cons 'width 80)))


      (if (not *pj/work-computer-p*)
          (progn
            (add-to-list 'default-frame-alist
                         (cons 'height (/ (- (x-display-pixel-height) 130)
                                          (frame-char-height))))

            (add-to-list 'default-frame-alist
                         (cons 'top (/ 130 2)))

            (add-to-list 'default-frame-alist
                         (cons 'left (/ (- (x-display-pixel-width) (* 120 (frame-char-width))) 2)))
            ))

      (add-to-list 'default-frame-alist
                   (cons 'font "Consolas-10"))

      (add-to-list 'default-frame-alist
                   (cons 'cursor-type 'box))

      ))

;; (setq default-frame-alist
;;           '((top . 0)(left . 0)
;;             (width . 85) (height . 48)
;;             (font . "Consolas-10")
;;             ))
;;   (setq default-frame-alist
;;         '((top . 0)(left . 0)
;;           (width . 85)(height . 35)
;;           (font . "Menlo-13")
;;           ))
;;   )
;; ))

;; (frame-height)
;; (frame-with
;; (frame-char-width)
;; (x-display-pixel-height)

;;   (if *pj/work-computer-p*

;;     (progn
;;       (setq default-frame-alist
;;             '((width . 100) (height . 50) (left-fringe . 0) (right-fringe . 8)))
;;       (setq initial-frame-alist
;;             '((top . 102) (left . 292))))

;;   (if *pj/home-computer-p*
;;       (progn
;;         (setq default-frame-alist
;;               '((width . 120) (height . 50) (left-fringe . 0) (right-fringe . 8)))

;;         (setq initial-frame-alist
;;               '((top . 8) (left . 312)))

;;         ;;(setq w32-use-w32-font-dialog nil)

;;         (custom-set-faces
;;          '(default ((t (:family "Consolas" :foundry "outline" :slant normal :weight normal :height 98 :width normal)))))
;;         )
;;     ))

;;
;; set frame and icon titles
;;
(setq frame-title-format '("%b"))
(setq icon-title-format '("%b"))

;;
;; smooth scroll through mouse wheel
;;
(setq mouse-wheel-scroll-amount '(4))
(setq mouse-wheel-progressive-speed nil)

(pj/dot-emacs-msg "end-of-WINDOW-SETUP")
