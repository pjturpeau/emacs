(pj/dot-emacs-msg "start-of-SPEEDBAR-SETUP")

(add-to-list 'load-path "~/Scripts/elisp/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

(setq neo-smart-open t)

(add-to-list 'load-path "~/Scripts/elisp/tabbar-master")
(require 'tabbar)

(setq tabbar-use-images nil)

;; (require'sr-speedbar)

;; (setq sr-speedbar-auto-refresh nil)
;; (setq speedbar-show-unknown-files t)
;; ;; (setq speedbar-use-images nil)
;; (setq sr-speedbar-right-side nil)

;; (make-face 'speedbar-face)
;; (set-face-font 'speedbar-face "Calibri-10")
;; (setq speedbar-mode-hook '(lambda () (buffer-face-set 'speedbar-face)))

;; (print (font-family-list))


;; (defun pj/speedbar-show-hide ()
;;   (interactive)
;;   (require 'speedbar)

;;   ;; disable automatic update (press <r> key instead)
;;   (setq speedbar-update-flag nil)
;;   (setq speedbar-hide-button-brackets-flag nil)
;;   (setq speedbar-indentation-width 2)
;;   (setq speedbar-show-unknown-files t)
;;   (setq speedbar-use-images nil)
;;   (setq speedbar-use-tool-tips-flag nil)

;;   (speedbar-add-supported-extension ".js")
;;   (speedbar-add-supported-extension ".php")

;;   (global-set-key "\C-cs" 'speedbar-get-focus)

;;   ;; this shall switch the focus from speedbar to the main frame,
;;   ;; but it does not work under Windows
;;   (add-hook 'speedbar-mode-hook
;;             '(lambda ()
;;                (interactive)
;;                (other-frame 0)))
;;   (speedbar))

(pj/dot-emacs-msg "end-of-SPEEDBAR-SETUP")
