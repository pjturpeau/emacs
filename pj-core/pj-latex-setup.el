(pj/dot-emacs-msg "start-of-LATEX-SETUP")

(defun pj/tex-to-accents ()
  "TeX accents to normal accents "
  (interactive "")
  (replace-string "\\'e" "�")
  (replace-string "\\`e" "�")
  (replace-string "\\`a" "�")
  (replace-string "\\^a" "�")
  (replace-string "\\^e" "�")
  (replace-string "\\^i{}" "�")
  (replace-string "\\^o" "�")
  (replace-string "\\^u" "�")
  (replace-string "\\\c{c}" "�"))

(defun pj/accents-to-tex ()
  "normal accents to TeX accents"
  (interactive "")
  (replace-string "�" "\\'e")
  (replace-string "�" "\\`e")
  (replace-string "�" "\\`a")
  (replace-string "�" "\\^a")
  (replace-string "�" "\\^e")
  (replace-string "�" "\\^\i{}")
  (replace-string "�" "\\^o")
  (replace-string "�" "\\^u")
  (replace-string "�" "\\c{c}"))

(pj/dot-emacs-msg "end-of-LATEX-SETUP")
