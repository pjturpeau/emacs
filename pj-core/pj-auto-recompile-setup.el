(pj/dot-emacs-msg "start-of-AUTO-RECOMPILE-SETUP")

(defun pj/auto-recompile-file-always ()
  (when (equal mode-name "Emacs-Lisp")
    (let ((maximum-compile-log-height 8))
      ;; pre-split for compile log buffer so that it does not modify the layout
      (set-window-buffer (split-window-vertically
                          (- (window-height) maximum-compile-log-height))
                         (get-buffer-create "*Compile-Log*"))
      ;; byte compile the buffer
      (byte-compile-file buffer-file-name)
      (let ((buf (get-buffer "*Compile-Log*")))
        ;; scroll to the end to see if there's an error
        (set-window-point (get-buffer-window buf) (buffer-size buf))
        ;; auto close the compile log window and recenter other window
        (run-at-time 1.0 nil (lambda (buf)
                               (delete-windows-on buf)
                               (recenter))

                     buf)))))

(pj/dot-emacs-msg "end-of-AUTO-RECOMPILE-SETUP")

;;
;; Credits:
;; --------
;;
;; I may have forgot some people over the years.
;;
;;  * Byte compile on buffer save original code from
;;    MichaelAbrahamSchulman's auto-recomp.el
;;    http://web.archive.org/web/20021216070753/http://kurukshetra.cjb.net/elisp/auto-recomp.el
;;
;;  * Other alternative ways (including mine) are available at
;;    http://www.emacswiki.org/cgi-bin/wiki/AutoRecompile
;;
